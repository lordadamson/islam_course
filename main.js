function see_more(id)
{
    document.getElementById(id).style.display='inline';
}

function insert_disussions()
{
    var discussion_tags = document.getElementsByName('discussion');

    for(var i = 0; i < discussion_tags.length; i++)
    {
        var content = discussion_tags[i].innerHTML;
        var template = `<button type="button" class="collapsible">
        <p>
        ${content}
        </p>
        </button>
        <div class="content">
        
        <textarea rows="6" cols="120"></textarea>
        
        <div style="height:1px; background: transparent;">
            <hr style="display:none;" />
        </div>
        
        <button onclick="see_more('see_more${i}')">حفظ</button>
        
        <div style="height:1px; background: transparent;">
            <hr style="display:none;" />
        </div>
        
        <a href="discussion.html" style="display:none;" id="see_more${i}">اطلع على نقاش الآخرين</a>
        
        </div>`;
        discussion_tags[i].innerHTML = template;
    }
}

insert_disussions();

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}